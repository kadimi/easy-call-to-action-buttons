<?php

/**
 * Plugin Name: Easy Call To Action Buttons
 * Plugin URI: http://example.com/plugins/easy-button
 * Description: Example description for the plugin.
 * Author: Company Name
 * Version: 1.0
 */

/**
 * @file
 * Easy Call To Action Buttons Plugin
 */

/**
 * Declare our shortcode with variations
 */
add_shortcode( 'easybutton', 'easy_button_shortcode' );
add_shortcode( 'easy-button', 'easy_button_shortcode' );
add_shortcode( 'easy_button', 'easy_button_shortcode' );

/**
 * Register, but don't load the plugin CSS
 * 
 * @return boolean Always true
 */
add_action( 'wp_enqueue_scripts', 'easy_button_action_register_css' );

/**
 * Registers the plugin css file so we can enqueue it at will
 */
function easy_button_action_register_css() {
	wp_register_style( 'easy-button', plugins_url( '/easy-button.css' , __FILE__ ) );
}

/**
 * The button generator for [easy-button]
 * 
 * @param mixed $atts empty string or an array of attributes
 * used as classes, $atts[ 'link' ] is used as the href
 * @param string $content the text inside the button
 * @return string the html code generated for the button
 */
function easy_button_shortcode( $atts, $content = '' ) {

	/**
	 * Default values for unrestricted attributes
	 *
	 * @name $attribute_defaults
	 * @var array
	 */
	$attribute_defaults = array(
		'link'  => '#',
	);

	/**
	 * Possible values for restricted attributes, default is first element for each (e.g: "_foo:foo")
	 *
	 * @name $attribute_options
	 * @var array
	 */
	$attribute_options = array(
		'_foo'  => array( 'foo', 'bar', 'baz' ),
		'color' => array( 'blue', 'red', 'orange', 'green', 'yellow', 'purple' ),
		'size'  => array( 'medium', 'tiny', 'small', 'large' ),
	);

	/**
	 * Populate defaults with restricted attributes defaults
	 */
	foreach ( $attribute_options as $attribute => $values ) {
		$attribute_defaults[ $attribute ] = $values[0];
	}

	/**
	 * Extract shortcode attributes
	 * 
	 * This populates non-provided attributes with defaults
	 * and sanitizes restricted attributes to allowed values
	 */
	$atts = shortcode_atts( $attribute_defaults, $atts );
	foreach ( $attribute_options as $attribute => $values ) {
		if ( empty ( $atts[ $attribute ] ) || ! in_array( $atts[ $attribute ], $values ) ) {
			$atts[ $attribute ] = $values[0];
		}
	}

	/**
	 * The button CSS classes
	 * 
	 * All shortcode attributes are appended to the button
	 * as CSS class except for the link attribute
	 * 
	 * @name $button_classes
	 * @var string
	 */
	$button_classes = array_diff_key( $atts, array_flip( array ( 'link' ) ) );

	/**
	 * Button HTML code
	 * 
	 * The button is a link  ur is escaped and 
	 * 
	 * @name $button_html
	 * @var string
	 */
	$button_html = sprintf( '<a href="%s" class="easy-button %s">%s</a>', esc_url( $atts[ 'link' ] ), implode( ' ', $button_classes ), $content );

	/**
	 * Load the CSS file for the plugin
	 */
	wp_enqueue_style( 'easy-button' );

	/**
	 * Mission complete
	 */
	return $button_html;
}
